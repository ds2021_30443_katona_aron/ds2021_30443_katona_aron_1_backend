import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    id("org.springframework.boot") version "2.5.6"
    id("io.spring.dependency-management") version "1.0.11.RELEASE"
    kotlin("jvm") version "1.5.31"
    kotlin("plugin.spring") version "1.5.31"
    kotlin("plugin.jpa") version "1.5.31"

    id("com.github.johnrengelman.processes") version "0.5.0"
    id("org.springdoc.openapi-gradle-plugin") version "1.3.3"
    id("org.openapi.generator") version "5.3.0"

    id("org.jlleitschuh.gradle.ktlint") version "10.2.0"

    idea
    jacoco
}

val majorVersion = 0
val minorVersion = 1
val fullVersion = "$majorVersion.$minorVersion.${System.getenv()["CI_PIPELINE_IID"] ?: "dev"}"

group = "com.katonaaron"
version = fullVersion
java.sourceCompatibility = JavaVersion.VERSION_1_8

configurations {
    compileOnly {
        extendsFrom(configurations.annotationProcessor.get())
    }
}

repositories {
    mavenLocal()
    mavenCentral()
}

dependencies {
    implementation("org.springframework.boot:spring-boot-starter-actuator")
    implementation("org.springframework.boot:spring-boot-starter-data-jpa")
    implementation("org.springframework.boot:spring-boot-starter-validation")
    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("org.springframework.boot:spring-boot-starter-security")

    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")

    runtimeOnly("io.jsonwebtoken:jjwt-impl:0.11.2")
    implementation("io.jsonwebtoken:jjwt-api:0.11.2")
    runtimeOnly("io.jsonwebtoken:jjwt-jackson:0.11.2")

    // https://mvnrepository.com/artifact/org.springdoc/springdoc-openapi-ui
    implementation("org.springdoc:springdoc-openapi-ui:1.5.12")
    // https://mvnrepository.com/artifact/org.springdoc/springdoc-openapi-kotlin
    implementation("org.springdoc:springdoc-openapi-kotlin:1.5.12")
    implementation("org.junit.jupiter:junit-jupiter:5.7.0")

    developmentOnly("org.springframework.boot:spring-boot-devtools")
    runtimeOnly("com.h2database:h2")
    runtimeOnly("org.postgresql:postgresql")
    annotationProcessor("org.springframework.boot:spring-boot-configuration-processor")
    testImplementation("org.springframework.boot:spring-boot-starter-test")
    testImplementation("org.mockito.kotlin:mockito-kotlin:4.0.0")
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs = listOf("-Xjsr305=strict")
        jvmTarget = "1.8"
    }
}

tasks.withType<Test> {
    useJUnitPlatform()
}

noArg {
    annotation("com.katonaaron.energy.util.NoArg")
}

openApi {
    forkProperties.set("-Dspring.profiles.active=openapi")
}

openApiGenerate {
    generatorName.set("typescript-angular")
    inputSpec.set("$buildDir/openapi.json")
    outputDir.set("$buildDir/typescript-angular/")
    configOptions.set(
        mapOf(
            "npmName" to "@energy-platform/energy-platform-api",
            "npmVersion" to fullVersion,
            "modelFileSuffix" to ".model"
        )
    )
    typeMappings.set(
        mapOf(
            "Date" to "Date",
            "date" to "Date",
            "DateTime" to "Date",
        )
    )
}

tasks.openApiGenerate {
    dependsOn("generateOpenApiDocs")
}

tasks.test {
    finalizedBy(tasks.jacocoTestReport) // report is always generated after tests run
}
tasks.jacocoTestReport {
    dependsOn(tasks.test) // tests are required to run before generating the report

    reports {
        xml.required.set(true)
    }
}

tasks.getByName<Jar>("jar") {
    enabled = false
}
