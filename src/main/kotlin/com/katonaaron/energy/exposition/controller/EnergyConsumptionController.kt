package com.katonaaron.energy.exposition.controller

import com.katonaaron.energy.application.consumption.RecordEnergyConsumption
import com.katonaaron.energy.application.consumption.ViewEnergyConsumptionOfClient
import com.katonaaron.energy.application.consumption.ViewEnergyConsumptionOfDevice
import com.katonaaron.energy.application.consumption.dto.EnergyConsumptionDto
import com.katonaaron.energy.domain.client.ClientId
import com.katonaaron.energy.domain.device.DeviceId
import com.katonaaron.energy.domain.sensor.SensorId
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.Parameter
import io.swagger.v3.oas.annotations.responses.ApiResponse
import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.format.annotation.DateTimeFormat
import org.springframework.http.ResponseEntity
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*
import java.time.LocalDate

@RestController
@Tag(name = "energy-consumption", description = "operations for viewing and recording energy consumption")
@Validated
class EnergyConsumptionController(
    private val recordEnergyConsumption: RecordEnergyConsumption,
    private val viewEnergyConsumptionOfDevice: ViewEnergyConsumptionOfDevice,
    private val viewEnergyConsumptionOfClient: ViewEnergyConsumptionOfClient
) {

    @PostMapping("/sensors/{sensorId}/recordConsumption")
    @Operation(
        summary = "Records a new energy consumption value of the attached device, measured by the sensor",
        responses = [
            ApiResponse(
                description = "Measurement recorded",
                responseCode = "204"
            )
        ]
    )
    fun recordEnergyConsumption(
        @PathVariable sensorId: String,
        @Parameter(description = "Energy consumption value in kWh. Must be smaller than the maximum value of the sensor and the maximum value of the device.")
        @RequestBody value: Long
    ): ResponseEntity<Any> {
        recordEnergyConsumption.recordEnergyConsumption(SensorId(sensorId), value)
        return ResponseEntity.noContent().build()
    }

    @GetMapping("/devices/{deviceId}/consumption")
    @Operation(
        summary = "Obtains the energy consumption history of the device",
        description = "Obtains the energy consumption history of the device. Can be filtered by the day."
    )
    fun viewEnergyConsumptionOfDevice(
        @PathVariable deviceId: String,
        @Parameter(
            description = "Specifies the day of measurement. If omitted, all the historical results are returned, from any day.",
            example = "2021-10-30"
        )
        @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
        @RequestParam(required = false) day: LocalDate?
    ): ResponseEntity<Collection<EnergyConsumptionDto>> =
        ResponseEntity.ok(
            day?.let {
                viewEnergyConsumptionOfDevice.viewHistoricalEnergyConsumptionOfDevice(DeviceId(deviceId), it)
            } ?: viewEnergyConsumptionOfDevice.viewHistoricalEnergyConsumptionOfDevice(DeviceId(deviceId))
        )

    @GetMapping("/clients/{clientId}/totalConsumption")
    @Operation(
        summary = "Obtains the total energy consumption of the client's devices"
    )
    fun viewTotalEnergyConsumptionOfClient(
        @PathVariable clientId: String
    ): ResponseEntity<Long?> =
        viewEnergyConsumptionOfClient.viewTotalEnergyConsumptionOfClient(ClientId(clientId))
            .let { ResponseEntity.ok(it) }
}
