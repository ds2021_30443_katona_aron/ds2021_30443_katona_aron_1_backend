package com.katonaaron.energy.exposition.controller

import com.katonaaron.energy.application.associations.AssociateSensorToDevice
import com.katonaaron.energy.application.associations.RemoveDeviceOfSensor
import com.katonaaron.energy.application.associations.ViewDeviceOfSensor
import com.katonaaron.energy.application.devices.ViewSingleDevice
import com.katonaaron.energy.application.devices.dto.DeviceDto
import com.katonaaron.energy.application.sensors.*
import com.katonaaron.energy.application.sensors.dto.CreateSensorDto
import com.katonaaron.energy.application.sensors.dto.SensorDto
import com.katonaaron.energy.application.sensors.dto.UpdateSensorDto
import com.katonaaron.energy.domain.device.DeviceId
import com.katonaaron.energy.domain.sensor.SensorId
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.media.Content
import io.swagger.v3.oas.annotations.responses.ApiResponse
import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@RestController
@RequestMapping("/sensors")
@Tag(name = "sensors", description = "operations on the sensors")
@Validated
class SensorController(
    private val createSensor: CreateSensor,
    private val viewSingleSensor: ViewSingleSensor,
    private val viewAllSensors: ViewAllSensors,
    private val updateSensor: UpdateSensor,
    private val deleteSensor: DeleteSensor,
    private val associateSensorToDevice: AssociateSensorToDevice,
    private val viewDeviceOfSensor: ViewDeviceOfSensor,
    private val viewSingleDevice: ViewSingleDevice,
    private val removeDeviceOfSensor: RemoveDeviceOfSensor
) {
    @PostMapping("/")
    @Operation(
        summary = "Creates a new sensor",
        responses = [
            ApiResponse(
                description = "Sensor created",
                responseCode = "201",
                content = [Content(mediaType = MediaType.APPLICATION_JSON_VALUE)]
            )
        ]
    )
    fun createSensor(@Valid @RequestBody dto: CreateSensorDto): ResponseEntity<SensorDto> =
        ResponseEntity(createSensor.createSensor(dto), HttpStatus.CREATED)

    @GetMapping("/")
    @Operation(
        summary = "Gets all the sensors",
    )
    fun viewAllSensors(): ResponseEntity<Collection<SensorDto>> =
        viewAllSensors.viewAllSensors()
            .let { ResponseEntity.ok(it) }

    @GetMapping("/{sensorId}")
    @Operation(
        summary = "View a sensor"
    )
    fun viewSingleSensor(
        @PathVariable sensorId: String,
    ): ResponseEntity<SensorDto> =
        viewSingleSensor.viewSingleSensor(SensorId(sensorId))
            .let { ResponseEntity.ok(it) }

    @PatchMapping("/{sensorId}")
    @Operation(
        summary = "Updates a sensor",
        description = "Updates a sensor. Only those fields are considered which are supplied."
    )
    fun updateSensor(
        @PathVariable sensorId: String,
        @Valid @RequestBody dto: UpdateSensorDto
    ): ResponseEntity<SensorDto> =
        updateSensor.updateSensor(SensorId(sensorId), dto)
            .let { ResponseEntity.ok(it) }

    @Operation(
        summary = "Deletes a sensor",
        responses = [
            ApiResponse(
                description = "Client deleted or not found",
                responseCode = "204"
            )
        ]
    )
    @DeleteMapping("/{sensorId}")
    fun deleteSensor(
        @PathVariable sensorId: String
    ): ResponseEntity<Any> {
        deleteSensor.deleteSensor(SensorId(sensorId))
        return ResponseEntity.noContent().build()
    }

    @GetMapping("/sensors/{sensorId}/device")
    @Operation(
        summary = "Gets the device of a sensor",
    )
    fun viewDeviceOfSensor(@PathVariable sensorId: String): ResponseEntity<DeviceDto?> =
        viewDeviceOfSensor.viewDeviceOfSensor(SensorId(sensorId))
            .let { ResponseEntity.ok(it) }

    @PutMapping("/sensors/{sensorId}/device")
    @Operation(
        summary = "Assigns an device to a sensor (bidirectional)",
    )
    fun assignDeviceToSensor(
        @RequestParam deviceId: String,
        @PathVariable sensorId: String
    ): ResponseEntity<DeviceDto> {
        associateSensorToDevice.associateSensorToDevice(SensorId(sensorId), DeviceId(deviceId))
        return ResponseEntity.ok(viewSingleDevice.viewSingleDevice(DeviceId(deviceId)))
    }

    @DeleteMapping("/sensors/{sensorId}/device")
    @Operation(
        summary = "Removes the sensor from the device",
        responses = [
            ApiResponse(
                description = "Sensor removed or device not found",
                responseCode = "204"
            )
        ]
    )
    fun removeDeviceOfSensor(
        @PathVariable sensorId: String
    ): ResponseEntity<Any> {
        removeDeviceOfSensor.removeDeviceOfSensor(SensorId(sensorId))
        return ResponseEntity.noContent().build()
    }
}
