package com.katonaaron.energy.exposition.controller

import com.katonaaron.energy.application.associations.*
import com.katonaaron.energy.application.clients.ViewSingleClient
import com.katonaaron.energy.application.clients.dto.ClientDto
import com.katonaaron.energy.application.devices.*
import com.katonaaron.energy.application.devices.dto.CreateDeviceDto
import com.katonaaron.energy.application.devices.dto.DeviceDto
import com.katonaaron.energy.application.devices.dto.UpdateDeviceDto
import com.katonaaron.energy.application.sensors.ViewSingleSensor
import com.katonaaron.energy.application.sensors.dto.SensorDto
import com.katonaaron.energy.domain.client.ClientId
import com.katonaaron.energy.domain.device.DeviceId
import com.katonaaron.energy.domain.sensor.SensorId
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.media.Content
import io.swagger.v3.oas.annotations.responses.ApiResponse
import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@RestController
@RequestMapping("/devices")
@Tag(name = "devices", description = "operations on the devices")
@Validated
class DeviceController(
    private val createDevice: CreateDevice,
    private val viewSingleDevice: ViewSingleDevice,
    private val viewAllDevices: ViewAllDevices,
    private val updateDevice: UpdateDevice,
    private val deleteDevice: DeleteDevice,
    private val associateDeviceToClient: AssociateDeviceToClient,
    private val viewSingleClient: ViewSingleClient,
    private val viewOwnerOfDevice: ViewOwnerOfDevice,
    private val removeOwnerOfDevice: RemoveOwnerOfDevice,
    private val associateSensorToDevice: AssociateSensorToDevice,
    private val viewSensorOfDevice: ViewSensorOfDevice,
    private val viewSingleSensor: ViewSingleSensor,
) {
    @PostMapping("/")
    @Operation(
        summary = "Creates a new device",
        responses = [
            ApiResponse(
                description = "Device created",
                responseCode = "201",
                content = [Content(mediaType = MediaType.APPLICATION_JSON_VALUE)]
            )
        ]
    )
    fun createDevice(@Valid @RequestBody dto: CreateDeviceDto): ResponseEntity<DeviceDto> =
        ResponseEntity(createDevice.createDevice(dto), HttpStatus.CREATED)

    @GetMapping("/")
    @Operation(
        summary = "Gets all the devices",
    )
    fun viewAllDevices(): ResponseEntity<Collection<DeviceDto>> =
        viewAllDevices.viewAllDevices()
            .let { ResponseEntity.ok(it) }

    @GetMapping("/{deviceId}")
    @Operation(
        summary = "View a device"
    )
    fun viewSingleDevice(
        @PathVariable deviceId: String,
    ): ResponseEntity<DeviceDto> =
        viewSingleDevice.viewSingleDevice(DeviceId(deviceId))
            .let { ResponseEntity.ok(it) }

    @PatchMapping("/{deviceId}")
    @Operation(
        summary = "Updates a device",
        description = "Updates a device. Only those fields are considered which are supplied."
    )
    fun updateDevice(
        @PathVariable deviceId: String,
        @Valid @RequestBody dto: UpdateDeviceDto
    ): ResponseEntity<DeviceDto> =
        updateDevice.updateDevice(DeviceId(deviceId), dto)
            .let { ResponseEntity.ok(it) }

    @Operation(
        summary = "Deletes a device",
        responses = [
            ApiResponse(
                description = "Client deleted or not found",
                responseCode = "204"
            )
        ]
    )
    @DeleteMapping("/{deviceId}")
    fun deleteDevice(
        @PathVariable deviceId: String
    ): ResponseEntity<Any> {
        deleteDevice.deleteDevice(DeviceId(deviceId))
        return ResponseEntity.noContent().build()
    }

    @GetMapping("/devices/{deviceId}/owner")
    @Operation(
        summary = "Gets the owner of a device",
    )
    fun viewOwnerOfDevice(@PathVariable deviceId: String): ResponseEntity<ClientDto?> =
        viewOwnerOfDevice.viewOwnerOfDevice(DeviceId(deviceId))
            .let { ResponseEntity.ok(it) }

    @PutMapping("/devices/{deviceId}/owner")
    @Operation(
        summary = "Assigns an owner to a device (bidirectional)",
    )
    fun assignOwnerToDevice(@PathVariable deviceId: String, @RequestParam clientId: String): ResponseEntity<ClientDto> {
        associateDeviceToClient.associateDeviceToClient(ClientId(clientId), DeviceId(deviceId))
        return ResponseEntity.ok(viewSingleClient.viewSingleClient(ClientId(clientId)))
    }

    @DeleteMapping("/devices/{deviceId}/owner")
    @Operation(
        summary = "Delete the association between the device and its owner (bidirectional)",
        responses = [
            ApiResponse(
                description = "Association deleted or not found",
                responseCode = "204"
            )
        ]
    )
    fun removeOwnerOfDevice(@PathVariable deviceId: String): ResponseEntity<Any> {
        removeOwnerOfDevice.removeOwnerOfDevice(DeviceId(deviceId))
        return ResponseEntity.noContent().build()
    }

    @GetMapping("/devices/{deviceId}/sensor")
    @Operation(
        summary = "Gets the sensor of a device",
    )
    fun viewSensorOfDevice(@PathVariable deviceId: String): ResponseEntity<SensorDto?> =
        viewSensorOfDevice.viewSensorOfDevice(DeviceId(deviceId))
            .let { ResponseEntity.ok(it) }

    @PutMapping("/devices/{deviceId}/sensor")
    @Operation(
        summary = "Assigns an sensor to a device (bidirectional)",
    )
    fun assignSensorToDevice(
        @PathVariable deviceId: String,
        @RequestParam sensorId: String
    ): ResponseEntity<SensorDto> {
        associateSensorToDevice.associateSensorToDevice(SensorId(sensorId), DeviceId(deviceId))
        return ResponseEntity.ok(viewSingleSensor.viewSingleSensor(SensorId(sensorId)))
    }
}
