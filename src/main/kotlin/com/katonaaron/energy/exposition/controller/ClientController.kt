package com.katonaaron.energy.exposition.controller

import com.katonaaron.energy.application.associations.AssociateDeviceToClient
import com.katonaaron.energy.application.associations.ViewDevicesOfClient
import com.katonaaron.energy.application.clients.*
import com.katonaaron.energy.application.clients.dto.ClientDto
import com.katonaaron.energy.application.clients.dto.CreateClientDto
import com.katonaaron.energy.application.clients.dto.UpdateClientDto
import com.katonaaron.energy.application.devices.dto.DeviceDto
import com.katonaaron.energy.domain.client.ClientId
import com.katonaaron.energy.domain.device.DeviceId
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.media.Content
import io.swagger.v3.oas.annotations.responses.ApiResponse
import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@RestController
@RequestMapping("/clients")
@Tag(name = "clients", description = "operations on the clients")
@Validated
class ClientController(
    private val createClient: CreateClient,
    private val viewAllClients: ViewAllClients,
    private val viewSingleClient: ViewSingleClient,
    private val updateClient: UpdateClient,
    private val deleteClient: DeleteClient,
    private val associateDeviceToClient: AssociateDeviceToClient,
    private val viewDevicesOfClient: ViewDevicesOfClient
) {

    @PostMapping("/")
    @Operation(
        summary = "Creates a new client",
        responses = [
            ApiResponse(
                description = "Client created",
                responseCode = "201",
                content = [Content(mediaType = MediaType.APPLICATION_JSON_VALUE)]
            )
        ]
    )
    fun createClient(@Valid @RequestBody dto: CreateClientDto): ResponseEntity<ClientDto> =
        ResponseEntity(createClient.createClient(dto), HttpStatus.CREATED)

    @GetMapping("/")
    @Operation(
        summary = "Gets all the clients",
    )
    fun viewAllClients(): ResponseEntity<Collection<ClientDto>> =
        viewAllClients.viewAllClients()
            .let { ResponseEntity.ok(it) }

    @GetMapping("/{clientId}")
    @Operation(
        summary = "View a client"
    )
    fun viewSingleClient(
        @PathVariable clientId: String,
    ): ResponseEntity<ClientDto> =
        viewSingleClient.viewSingleClient(ClientId(clientId))
            .let { ResponseEntity.ok(it) }

    @PatchMapping("/{clientId}")
    @Operation(
        summary = "Updates a client",
        description = "Updates a client. Only those fields are considered which are supplied."
    )
    fun updateClient(
        @PathVariable clientId: String,
        @Valid @RequestBody dto: UpdateClientDto
    ): ResponseEntity<ClientDto> =
        updateClient.updateClient(ClientId(clientId), dto)
            .let { ResponseEntity.ok(it) }

    @Operation(
        summary = "Deletes a client",
        responses = [
            ApiResponse(
                description = "Client deleted or not found",
                responseCode = "204"
            )
        ]
    )
    @DeleteMapping("/{clientId}")
    fun deleteClient(
        @PathVariable clientId: String
    ): ResponseEntity<Any> {
        deleteClient.deleteClient(ClientId(clientId))
        return ResponseEntity.noContent().build()
    }

    @GetMapping("/clients/{clientId}/devices")
    @Operation(
        summary = "Gets the devices of a client",
    )
    fun viewDevicesOfClient(@PathVariable clientId: String): ResponseEntity<Collection<DeviceDto>> =
        viewDevicesOfClient.viewDevicesOfClient(ClientId(clientId))
            .let { ResponseEntity.ok(it) }

    @PutMapping("/clients/{clientId}/devices")
    @Operation(
        summary = "Assigns a device to a client (bidirectional)",
    )
    fun assignDeviceToClient(
        @RequestParam deviceId: String,
        @PathVariable clientId: String
    ): ResponseEntity<Collection<DeviceDto>> {
        associateDeviceToClient.associateDeviceToClient(ClientId(clientId), DeviceId(deviceId))
        return viewDevicesOfClient(clientId)
    }
}
