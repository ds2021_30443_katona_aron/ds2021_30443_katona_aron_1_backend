package com.katonaaron.energy.exposition.handlers.exceptions.model

import org.springframework.http.HttpStatus

class ResourceNotFoundException(resource: String?) : CustomException(MESSAGE, httpStatus, resource, emptyList()) {
    companion object {
        private const val MESSAGE = "Resource not found!"
        private val httpStatus = HttpStatus.NOT_FOUND
    }
}
