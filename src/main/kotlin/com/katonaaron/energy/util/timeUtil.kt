package com.katonaaron.energy.util

import java.time.Instant
import java.time.LocalDate
import java.time.ZoneId
import java.time.ZonedDateTime

fun Instant.toLocalDate(): LocalDate = LocalDate.ofInstant(this, ZoneId.systemDefault())

fun Instant.toZonedDateTime(): ZonedDateTime = atZone(ZoneId.systemDefault())
