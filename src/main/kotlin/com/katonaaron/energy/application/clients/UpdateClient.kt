package com.katonaaron.energy.application.clients

import com.katonaaron.energy.application.ApplicationService
import com.katonaaron.energy.application.clients.dto.ClientDto
import com.katonaaron.energy.application.clients.dto.UpdateClientDto
import com.katonaaron.energy.application.clients.dto.toDto
import com.katonaaron.energy.domain.DDD
import com.katonaaron.energy.domain.client.Client
import com.katonaaron.energy.domain.client.ClientId
import com.katonaaron.energy.domain.client.Clients
import com.katonaaron.energy.exposition.handlers.exceptions.model.ResourceNotFoundException

@DDD.ApplicationService
@ApplicationService
class UpdateClient(
    private val clients: Clients
) {

    fun updateClient(id: ClientId, dto: UpdateClientDto): ClientDto {
        val client = clients.findById(id)
            ?: throw ResourceNotFoundException(Client::class.simpleName + " with id: " + id)

        dto.address?.let { client.address = it }
        dto.birthDate?.let { client.birthDate = it }
        dto.name?.let { client.name = it }

        return clients.save(client).toDto()
    }
}
