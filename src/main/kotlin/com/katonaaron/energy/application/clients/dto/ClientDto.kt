package com.katonaaron.energy.application.clients.dto

import com.fasterxml.jackson.annotation.JsonFormat
import com.katonaaron.energy.domain.client.Client
import io.swagger.v3.oas.annotations.media.Schema
import java.time.LocalDate

@Schema(name = "Client")
class ClientDto(
    val id: String,
    val username: String,
    val name: String,
    @JsonFormat(pattern = "yyyy-MM-dd")
    val birthDate: LocalDate,
    val address: String
)

fun Client.toDto() =
    ClientDto(
        id = id.value,
        name = name,
        birthDate = birthDate,
        address = address,
        username = account.username
    )
