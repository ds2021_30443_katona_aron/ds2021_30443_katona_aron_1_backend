package com.katonaaron.energy.application.clients

import com.katonaaron.energy.application.ApplicationService
import com.katonaaron.energy.application.clients.dto.ClientDto
import com.katonaaron.energy.application.clients.dto.toDto
import com.katonaaron.energy.domain.DDD
import com.katonaaron.energy.domain.client.Client
import com.katonaaron.energy.domain.client.Clients

@DDD.ApplicationService
@ApplicationService
class ViewAllClients(
    private val clients: Clients
) {
    fun viewAllClients(): Collection<ClientDto> =
        clients.findAll().map(Client::toDto)
}
