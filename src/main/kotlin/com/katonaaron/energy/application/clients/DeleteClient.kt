package com.katonaaron.energy.application.clients

import com.katonaaron.energy.application.ApplicationService
import com.katonaaron.energy.domain.DDD
import com.katonaaron.energy.domain.client.ClientId
import com.katonaaron.energy.domain.client.Clients

@DDD.ApplicationService
@ApplicationService
class DeleteClient(
    private val clients: Clients
) {

    fun deleteClient(id: ClientId) {
        val client = clients.findById(id) ?: return

        if (client.hasAnyDevice()) {
            client.removeAllDevices()
        }

        clients.delete(client)
    }
}
