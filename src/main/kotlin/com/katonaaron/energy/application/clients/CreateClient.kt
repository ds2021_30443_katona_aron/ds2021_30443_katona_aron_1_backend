package com.katonaaron.energy.application.clients

import com.katonaaron.energy.application.ApplicationService
import com.katonaaron.energy.application.clients.dto.ClientDto
import com.katonaaron.energy.application.clients.dto.CreateClientDto
import com.katonaaron.energy.application.clients.dto.toDto
import com.katonaaron.energy.application.useraccount.CreateUserAccount
import com.katonaaron.energy.domain.DDD
import com.katonaaron.energy.domain.client.Client
import com.katonaaron.energy.domain.client.Clients
import com.katonaaron.energy.domain.useraccount.UserRole

@DDD.ApplicationService
@ApplicationService
class CreateClient(
    private val clients: Clients,
    private val createUserAccount: CreateUserAccount
) {

    fun createClient(dto: CreateClientDto): ClientDto {
        val account = createUserAccount.createUserAccount(
            username = dto.username,
            password = dto.password,
            role = UserRole.ROLE_CLIENT
        )

        val id = clients.nextIdentity()
        val client = Client(
            id,
            dto.name,
            dto.birthDate,
            dto.address,
            account
        )
        val savedClient = clients.save(client)

        return savedClient.toDto()
    }
}
