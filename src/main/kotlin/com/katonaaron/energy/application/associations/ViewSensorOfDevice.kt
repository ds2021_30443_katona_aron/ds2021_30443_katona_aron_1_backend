package com.katonaaron.energy.application.associations

import com.katonaaron.energy.application.ApplicationService
import com.katonaaron.energy.application.sensors.dto.SensorDto
import com.katonaaron.energy.application.sensors.dto.toDto
import com.katonaaron.energy.domain.DDD
import com.katonaaron.energy.domain.device.Device
import com.katonaaron.energy.domain.device.DeviceId
import com.katonaaron.energy.domain.device.Devices
import com.katonaaron.energy.exposition.handlers.exceptions.model.ResourceNotFoundException

@DDD.ApplicationService
@ApplicationService
class ViewSensorOfDevice(
    private val devices: Devices
) {
    fun viewSensorOfDevice(deviceId: DeviceId): SensorDto? {
        val device = devices.findById(deviceId)
            ?: throw ResourceNotFoundException("${Device::class.simpleName} with id $deviceId")
        return device.sensor?.toDto()
    }
}
