package com.katonaaron.energy.application.associations

import com.katonaaron.energy.application.ApplicationService
import com.katonaaron.energy.domain.DDD
import com.katonaaron.energy.domain.device.Device
import com.katonaaron.energy.domain.device.DeviceId
import com.katonaaron.energy.domain.device.Devices
import com.katonaaron.energy.domain.sensor.Sensor
import com.katonaaron.energy.domain.sensor.SensorId
import com.katonaaron.energy.domain.sensor.Sensors
import com.katonaaron.energy.exposition.handlers.exceptions.model.ResourceNotFoundException

@DDD.ApplicationService
@ApplicationService
class AssociateSensorToDevice(
    private val sensors: Sensors,
    private val devices: Devices
) {

    fun associateSensorToDevice(sensorId: SensorId, deviceId: DeviceId) {
        val sensor = sensors.findById(sensorId)
            ?: throw ResourceNotFoundException("${Sensor::class.simpleName} with id $sensorId")
        val device = devices.findById(deviceId)
            ?: throw ResourceNotFoundException("${Device::class.simpleName} with id $deviceId")

        if (sensor.device == device) {
            return
        }

        if (sensor.isAttachedToDevice()) {
            sensor.detachFromDevice()
        }

        if (device.hasSensor()) {
            device.removeSensor()
        }

        device.attachSensor(sensor)
        sensors.save(sensor)
    }
}
