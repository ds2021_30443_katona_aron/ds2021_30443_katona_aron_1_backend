package com.katonaaron.energy.application.associations

import com.katonaaron.energy.application.ApplicationService
import com.katonaaron.energy.domain.DDD
import com.katonaaron.energy.domain.client.Client
import com.katonaaron.energy.domain.client.ClientId
import com.katonaaron.energy.domain.client.Clients
import com.katonaaron.energy.domain.device.Device
import com.katonaaron.energy.domain.device.DeviceId
import com.katonaaron.energy.domain.device.Devices
import com.katonaaron.energy.exposition.handlers.exceptions.model.ResourceNotFoundException

@DDD.ApplicationService
@ApplicationService
class AssociateDeviceToClient(
    private val clients: Clients,
    private val devices: Devices
) {

    fun associateDeviceToClient(clientId: ClientId, deviceId: DeviceId) {
        val device = devices.findById(deviceId)
            ?: throw ResourceNotFoundException("${Device::class.simpleName} with id $deviceId")
        val client = clients.findById(clientId)
            ?: throw ResourceNotFoundException("${Client::class.simpleName} with id $clientId")

        if (device.hasOwner()) {
            if (device.owner == client) {
                return
            } else {
                device.removeOwner()
            }
        }

        device.setOwner(client)
        devices.save(device)
    }
}
