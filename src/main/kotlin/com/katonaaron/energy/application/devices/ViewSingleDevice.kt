package com.katonaaron.energy.application.devices

import com.katonaaron.energy.application.ApplicationService
import com.katonaaron.energy.application.devices.dto.DeviceDto
import com.katonaaron.energy.application.devices.dto.toDto
import com.katonaaron.energy.domain.DDD
import com.katonaaron.energy.domain.device.Device
import com.katonaaron.energy.domain.device.DeviceId
import com.katonaaron.energy.domain.device.Devices
import com.katonaaron.energy.exposition.handlers.exceptions.model.ResourceNotFoundException

@DDD.ApplicationService
@ApplicationService
class ViewSingleDevice(
    private val devices: Devices
) {
    fun viewSingleDevice(id: DeviceId): DeviceDto = devices.findById(id)?.toDto()
        ?: throw ResourceNotFoundException(Device::class.simpleName + " with id: " + id)
}
