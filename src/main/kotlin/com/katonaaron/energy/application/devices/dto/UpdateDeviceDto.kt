package com.katonaaron.energy.application.devices.dto

import io.swagger.v3.oas.annotations.media.Schema
import javax.validation.constraints.Size

data class UpdateDeviceDto(
    @field:Size(min = 3, max = 255, message = "The description must be at least 3, at most 255 characters long")
    @Schema(example = "Smart heater")
    val description: String?,
    @Schema(example = "John str. 21")
    val address: String?,
    @Schema(example = "1230", description = "Maximum possible energy consumption in kWh")
    val maxEnergyConsumption: Long?,
    @Schema(example = "523", description = "Baseline energy consumption in kWh")
    val baselineEnergyConsumption: Long?
)
