package com.katonaaron.energy.application.devices

import com.katonaaron.energy.application.ApplicationService
import com.katonaaron.energy.application.devices.dto.DeviceDto
import com.katonaaron.energy.application.devices.dto.toDto
import com.katonaaron.energy.domain.DDD
import com.katonaaron.energy.domain.device.Device
import com.katonaaron.energy.domain.device.Devices

@DDD.ApplicationService
@ApplicationService
class ViewAllDevices(
    private val devices: Devices
) {
    fun viewAllDevices(): Collection<DeviceDto> = devices.findAll().map(Device::toDto)
}
