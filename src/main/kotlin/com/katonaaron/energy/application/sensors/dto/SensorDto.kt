package com.katonaaron.energy.application.sensors.dto

import com.katonaaron.energy.domain.energy.EnergyUnit
import com.katonaaron.energy.domain.sensor.Sensor
import io.swagger.v3.oas.annotations.media.Schema

@Schema(name = "Sensor")
data class SensorDto(
    val id: String,
    val description: String,
    val maxValue: Long
)

fun Sensor.toDto(): SensorDto = SensorDto(
    id = id.value,
    description = description,
    maxValue = maxValue.amount(EnergyUnit.kWh)
)
