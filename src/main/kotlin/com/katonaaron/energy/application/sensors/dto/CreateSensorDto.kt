package com.katonaaron.energy.application.sensors.dto

import io.swagger.v3.oas.annotations.media.Schema
import javax.validation.constraints.NotBlank
import javax.validation.constraints.Size

data class CreateSensorDto(
    @field:Size(min = 3, max = 255, message = "The description must be at least 3, at most 255 characters long")
    @field:NotBlank(message = "Must provide a description")
    @Schema(example = "SR-125")
    val description: String,
    @Schema(example = "1230", description = "Maximum possible value in kWh")
    val maxValue: Long
)
