package com.katonaaron.energy.application.sensors

import com.katonaaron.energy.application.ApplicationService
import com.katonaaron.energy.domain.DDD
import com.katonaaron.energy.domain.sensor.SensorId
import com.katonaaron.energy.domain.sensor.Sensors

@DDD.ApplicationService
@ApplicationService
class DeleteSensor(
    private val sensors: Sensors
) {

    fun deleteSensor(id: SensorId) {
        val sensor = sensors.findById(id) ?: return

        if (sensor.isAttachedToDevice()) {
            sensor.detachFromDevice()
        }

        sensors.delete(sensor)
    }
}
