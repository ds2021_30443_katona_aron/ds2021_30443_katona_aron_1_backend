package com.katonaaron.energy.application.sensors

import com.katonaaron.energy.application.ApplicationService
import com.katonaaron.energy.application.sensors.dto.CreateSensorDto
import com.katonaaron.energy.application.sensors.dto.SensorDto
import com.katonaaron.energy.application.sensors.dto.toDto
import com.katonaaron.energy.domain.DDD
import com.katonaaron.energy.domain.energy.Energy
import com.katonaaron.energy.domain.energy.EnergyUnit
import com.katonaaron.energy.domain.sensor.Sensor
import com.katonaaron.energy.domain.sensor.Sensors

@DDD.ApplicationService
@ApplicationService
class CreateSensor(
    private val sensors: Sensors
) {
    fun createSensor(dto: CreateSensorDto): SensorDto {
        val sensor = Sensor(
            id = sensors.nextIdentity(),
            description = dto.description,
            maxValue = Energy(dto.maxValue, EnergyUnit.kWh),
        )
        return sensors.save(sensor).toDto()
    }
}
