package com.katonaaron.energy.application.sensors.dto

import io.swagger.v3.oas.annotations.media.Schema
import javax.validation.constraints.Size

data class UpdateSensorDto(
    @field:Size(min = 3, max = 255, message = "The description must be at least 3, at most 255 characters long")
    @Schema(example = "SR-125")
    val description: String?,
    @Schema(example = "1230", description = "Maximum possible value in kWh")
    val maxValue: Long?
)
