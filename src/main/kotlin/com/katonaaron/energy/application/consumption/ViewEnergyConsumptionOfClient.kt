package com.katonaaron.energy.application.consumption

import com.katonaaron.energy.application.ApplicationService
import com.katonaaron.energy.domain.DDD
import com.katonaaron.energy.domain.client.Client
import com.katonaaron.energy.domain.client.ClientId
import com.katonaaron.energy.domain.client.Clients
import com.katonaaron.energy.domain.energy.Energy
import com.katonaaron.energy.domain.energy.EnergyUnit
import com.katonaaron.energy.exposition.handlers.exceptions.model.ResourceNotFoundException

@DDD.ApplicationService
@ApplicationService
class ViewEnergyConsumptionOfClient(
    private val clients: Clients
) {

    fun viewTotalEnergyConsumptionOfClient(clientId: ClientId): Long? {
        val client = clients.findById(clientId)
            ?: throw ResourceNotFoundException(Client::class.simpleName + " with id: " + clientId)
        return client.devices
            .mapNotNull { it.currentEnergyConsumption?.value }
            .reduceOrNull(Energy::plus)
            ?.amount(EnergyUnit.kWh)
    }
}
