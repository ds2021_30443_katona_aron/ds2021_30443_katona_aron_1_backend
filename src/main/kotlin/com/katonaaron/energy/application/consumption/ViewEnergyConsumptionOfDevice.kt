package com.katonaaron.energy.application.consumption

import com.katonaaron.energy.application.ApplicationService
import com.katonaaron.energy.application.consumption.dto.EnergyConsumptionDto
import com.katonaaron.energy.application.consumption.dto.toDto
import com.katonaaron.energy.domain.DDD
import com.katonaaron.energy.domain.device.Device
import com.katonaaron.energy.domain.device.DeviceId
import com.katonaaron.energy.domain.device.Devices
import com.katonaaron.energy.exposition.handlers.exceptions.model.ResourceNotFoundException
import com.katonaaron.energy.util.toLocalDate
import java.time.LocalDate

@DDD.ApplicationService
@ApplicationService
class ViewEnergyConsumptionOfDevice(
    private val devices: Devices
) {
    fun viewCurrentEnergyConsumptionOfDevice(deviceId: DeviceId): EnergyConsumptionDto? =
        findDevice(deviceId).currentEnergyConsumption?.toDto()

    fun viewHistoricalEnergyConsumptionOfDevice(deviceId: DeviceId): Collection<EnergyConsumptionDto> =
        findDevice(deviceId).energyConsumptionHistory.map { it.toDto() }

    fun viewHistoricalEnergyConsumptionOfDevice(deviceId: DeviceId, day: LocalDate): Collection<EnergyConsumptionDto> =
        findDevice(deviceId).energyConsumptionHistory
            .filter { it.timestamp.toLocalDate() == day }
            .map { it.toDto() }

    private fun findDevice(deviceId: DeviceId): Device = devices.findById(deviceId)
        ?: throw ResourceNotFoundException(Device::class.simpleName + " with id: " + deviceId)
}
