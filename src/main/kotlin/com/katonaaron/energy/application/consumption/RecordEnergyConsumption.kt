package com.katonaaron.energy.application.consumption

import com.katonaaron.energy.application.ApplicationService
import com.katonaaron.energy.domain.DDD
import com.katonaaron.energy.domain.energy.Energy
import com.katonaaron.energy.domain.energy.EnergyUnit
import com.katonaaron.energy.domain.sensor.Sensor
import com.katonaaron.energy.domain.sensor.SensorId
import com.katonaaron.energy.domain.sensor.Sensors
import com.katonaaron.energy.exposition.handlers.exceptions.model.ResourceNotFoundException

@DDD.ApplicationService
@ApplicationService
class RecordEnergyConsumption(
    private val sensors: Sensors
) {
    fun recordEnergyConsumption(sensorId: SensorId, value: Long) {
        val sensor = sensors.findById(sensorId)
            ?: throw ResourceNotFoundException(Sensor::class.simpleName + " with id: " + sensorId)

        sensor.recordEnergyConsumption(Energy(value, EnergyUnit.kWh))
    }
}
