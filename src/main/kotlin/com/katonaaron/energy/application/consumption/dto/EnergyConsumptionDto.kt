package com.katonaaron.energy.application.consumption.dto

import com.katonaaron.energy.domain.device.EnergyConsumption
import com.katonaaron.energy.domain.energy.EnergyUnit
import com.katonaaron.energy.util.toZonedDateTime
import io.swagger.v3.oas.annotations.media.Schema
import java.time.ZonedDateTime

@Schema(name = "EnergyConsumption")
data class EnergyConsumptionDto(
    val timestamp: ZonedDateTime,
    val consumption: Long
)

fun EnergyConsumption.toDto(): EnergyConsumptionDto =
    EnergyConsumptionDto(
        timestamp = timestamp.toZonedDateTime(),
        consumption = value.amount(EnergyUnit.kWh)
    )
