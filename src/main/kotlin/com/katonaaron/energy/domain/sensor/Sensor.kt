package com.katonaaron.energy.domain.sensor

import com.katonaaron.energy.domain.DDD
import com.katonaaron.energy.domain.DomainConstraintValidationException
import com.katonaaron.energy.domain.Validatable
import com.katonaaron.energy.domain.device.Device
import com.katonaaron.energy.domain.energy.Energy
import java.util.*
import javax.persistence.*
import javax.validation.constraints.NotBlank
import javax.validation.constraints.Size

@DDD.DomainEntity
@Entity
class Sensor(
    @EmbeddedId
    @AttributeOverride(name = "value", column = Column(name = "id"))
    val id: SensorId,
    description: String,
    maxValue: Energy,
) : Validatable {

    @Size(min = 3, max = 255, message = "The description must be at least 3, at most 255 characters long")
    @NotBlank(message = "Must provide a description")
    var description: String = description
        set(value) {
            field = value
            validate(this)
        }

    @Embedded
    @AttributeOverride(name = "amountSI", column = Column(name = "max_value"))
//    @Positive
    var maxValue: Energy = maxValue
        set(value) {
            field = value
            validate(this)
        }

    @OneToOne(
        mappedBy = "sensor"
    )
    var device: Device? = null
        private set

    fun attachToDevice(device: Device) {
        if (this.device != null) {
            if (this.device == device) {
                return
            } else {
                throw DomainConstraintValidationException("Sensor already has a device")
            }
        }
        this.device = device
        device.attachSensor(this)
        validate(this)
    }

    fun isAttachedToDevice(): Boolean = device != null

    fun detachFromDevice() {
        device?.let {
            device = null
            it.removeSensor()
            validate(this)
        }
    }

    fun recordEnergyConsumption(value: Energy) {
        if (value > maxValue) {
            throw DomainConstraintValidationException("Energy value must not be greater the maximum possible value of the sensor")
        }
        device?.recordEnergyConsumption(value)
            ?: throw DomainConstraintValidationException("Sensor not connected to device")
    }

    init {
        validate(this)
    }

    override fun equals(other: Any?): Boolean {
        return when {
            this === other -> true
            other is Sensor -> id == other.id
            else -> false
        }
    }

    override fun hashCode(): Int = Objects.hash(id)
}
