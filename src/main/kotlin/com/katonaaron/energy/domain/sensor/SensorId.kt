package com.katonaaron.energy.domain.sensor

import com.katonaaron.energy.domain.DDD
import com.katonaaron.energy.domain.Validatable
import java.io.Serializable
import javax.persistence.Embeddable
import javax.validation.constraints.NotBlank

@Suppress("JpaAttributeMemberSignatureInspection")
@DDD.ValueObjectId
@Embeddable
data class SensorId(
    @NotBlank
    val value: String
) : Validatable, Serializable {
    init {
        validate(this)
    }

    override fun toString(): String {
        return value
    }
}
