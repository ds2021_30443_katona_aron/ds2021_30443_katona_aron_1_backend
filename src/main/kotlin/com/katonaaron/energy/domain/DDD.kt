package com.katonaaron.energy.domain

annotation class DDD {
    annotation class DomainEntity
    annotation class ValueObject
    annotation class ValueObjectId
    annotation class Aggregate
    annotation class AggregateRoot
    annotation class DomainService
    annotation class DomainEvent
    annotation class InfrastructureService
    annotation class InfrastructureServiceImpl
    annotation class Repository
    annotation class RepositoryImpl
    annotation class ApplicationService
}
