package com.katonaaron.energy.domain.useraccount

interface UserAccounts {
    fun nextIdentity(): UserAccountId

    fun save(account: UserAccount): UserAccount

    fun findAll(): Collection<UserAccount>

    fun findById(id: UserAccountId): UserAccount?

    fun findByUsername(username: String): UserAccount?

    fun delete(account: UserAccount)

    fun existsById(id: UserAccountId): Boolean

    fun getNumberOfAccounts(): Long

    fun hasNoAccount(): Boolean = getNumberOfAccounts() == 0L
}
