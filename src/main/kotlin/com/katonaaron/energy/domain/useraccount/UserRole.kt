package com.katonaaron.energy.domain.useraccount

enum class UserRole {
    ROLE_ADMIN,
    ROLE_CLIENT
}
