package com.katonaaron.energy.domain.device

import com.katonaaron.energy.domain.DDD
import com.katonaaron.energy.domain.DomainConstraintValidationException
import com.katonaaron.energy.domain.Validatable
import com.katonaaron.energy.domain.client.Client
import com.katonaaron.energy.domain.energy.Energy
import com.katonaaron.energy.domain.sensor.Sensor
import com.katonaaron.energy.util.append
import java.time.Instant
import java.util.*
import javax.persistence.*
import javax.validation.constraints.NotBlank
import javax.validation.constraints.Size

@DDD.DomainEntity
@Entity
class Device(
    @EmbeddedId
    @AttributeOverride(name = "value", column = Column(name = "id"))
    val id: DeviceId,
    description: String,
    address: String,
    maxEnergyConsumption: Energy,
    baselineEnergyConsumption: Energy
) : Validatable {

    @Size(min = 3, max = 255, message = "The description must be at least 3, at most 255 characters long")
    @NotBlank(message = "Must provide a description")
    var description: String = description
        set(value) {
            field = value
            validate(this)
        }

    @NotBlank(message = "Must provide an address")
    var address: String = address
        set(value) {
            field = value
            validate(this)
        }

    @Embedded
    @AttributeOverride(name = "amountSI", column = Column(name = "max_energy_consumption"))
//    @PositiveOrZero
    var maxEnergyConsumption: Energy = maxEnergyConsumption
        set(value) {
            field = value
            validate(this)
        }

    @Embedded
    @AttributeOverride(name = "amountSI", column = Column(name = "base_energy_consumption"))
//    @Positive
    var baselineEnergyConsumption: Energy = baselineEnergyConsumption
        set(value) {
            field = value
            validate(this)
        }

    @OneToOne(
        fetch = FetchType.LAZY,
        optional = true
    )
    var sensor: Sensor? = null
        private set

    fun attachSensor(sensor: Sensor) {
        if (this.sensor != null) {
            if (this.sensor == sensor) {
                return
            } else {
                throw DomainConstraintValidationException("Device already has a sensor")
            }
        }
        this.sensor = sensor
        sensor.attachToDevice(this)
        validate(this)
    }

    fun hasSensor(): Boolean = sensor != null

    fun removeSensor() {
        sensor?.let {
            sensor = null
            it.detachFromDevice()
            validate(this)
        }
    }

    @ManyToOne(fetch = FetchType.LAZY)
    var owner: Client? = null
        private set

    fun setOwner(owner: Client) {
        if (this.owner != null) {
            if (this.owner == owner) {
                return
            } else {
                throw DomainConstraintValidationException("Device already has an owner")
            }
        }
        this.owner = owner
        owner.assignDevice(this)
        validate(this)
    }

    fun hasOwner(): Boolean = owner != null

    fun removeOwner() {
        owner?.let {
            owner = null
            it.removeDevice(this)
            validate(this)
        }
    }

    @ElementCollection(fetch = FetchType.LAZY)
    @OrderBy("timestamp ASC")
    private val _energyConsumptionHistory: MutableList<EnergyConsumption> = mutableListOf()
    val energyConsumptionHistory: Collection<EnergyConsumption>
        get() = _energyConsumptionHistory

    val currentEnergyConsumption: EnergyConsumption?
        get() = _energyConsumptionHistory.lastOrNull()

    fun recordEnergyConsumption(value: Energy) {
        if (!value.isPositiveOrZero()) {
            throw DomainConstraintValidationException("The energy consumption must be larger than 0")
        }
        if (value > maxEnergyConsumption) {
            throw DomainConstraintValidationException("The energy consumption must be smaller than the maximum possible value")
        }

        currentEnergyConsumption?.let {
            if (value < it.value) {
                throw DomainConstraintValidationException("The energy consumption must be larger than or equal with the current one")
            }
        }

        _energyConsumptionHistory.append(EnergyConsumption(Instant.now(), value))
    }

    init {
        validate(this)
    }

    override fun equals(other: Any?): Boolean {
        return when {
            this === other -> true
            other is Device -> id == other.id
            else -> false
        }
    }

    override fun hashCode(): Int = Objects.hash(id)
}
