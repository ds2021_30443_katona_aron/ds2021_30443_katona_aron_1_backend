package com.katonaaron.energy.domain.device

import com.katonaaron.energy.domain.DDD
import com.katonaaron.energy.domain.energy.Energy
import java.time.Instant
import javax.persistence.*

@DDD.ValueObject
@Embeddable
@Access(AccessType.FIELD)
data class EnergyConsumption(
    val timestamp: Instant,
    @Embedded
    @AttributeOverride(name = "amountSI", column = Column(name = "value"))
    val value: Energy
)
