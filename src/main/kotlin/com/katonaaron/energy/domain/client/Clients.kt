package com.katonaaron.energy.domain.client

import com.katonaaron.energy.domain.DDD

@DDD.Repository
interface Clients {
    fun nextIdentity(): ClientId

    fun save(client: Client): Client

    fun findAll(): Collection<Client>

    fun findById(id: ClientId): Client?

    fun delete(client: Client)
}
