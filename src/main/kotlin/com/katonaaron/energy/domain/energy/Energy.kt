package com.katonaaron.energy.domain.energy

import com.katonaaron.energy.domain.DDD
import org.springframework.stereotype.Component
import javax.persistence.Access
import javax.persistence.AccessType
import javax.persistence.Embeddable
import javax.persistence.Transient
import javax.validation.ConstraintValidator
import javax.validation.ConstraintValidatorContext
import javax.validation.constraints.Positive
import javax.validation.constraints.PositiveOrZero

@DDD.ValueObject
@Embeddable
@Access(AccessType.FIELD)
class Energy(amount: Long, unit: EnergyUnit) : Comparable<Energy> {
    companion object {
        val SI_UNIT = EnergyUnit.J

        private const val kWh_IN_J = 3_600_000

        fun convertToSI(amount: Long, from: EnergyUnit): Long {
            return when (from) {
                EnergyUnit.J -> amount
                EnergyUnit.kWh -> amount * kWh_IN_J
            }
        }

        fun convertFromSI(amount: Long, to: EnergyUnit): Long {
            return when (to) {
                EnergyUnit.J -> amount
                EnergyUnit.kWh -> amount / kWh_IN_J
            }
        }
    }

    private val amountSI: Long = convertToSI(amount, unit)

    fun amount(unit: EnergyUnit): Long = convertFromSI(amountSI, unit)

    @Transient
    fun isPositiveOrZero(): Boolean = amountSI >= 0

    @Transient
    fun isPositive(): Boolean = amountSI > 0

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is Energy) return false

        if (amountSI != other.amountSI) return false

        return true
    }

    override fun hashCode(): Int {
        return amountSI.hashCode()
    }

    override fun compareTo(other: Energy): Int {
        return amountSI.compareTo(other.amountSI)
    }

    operator fun plus(increment: Energy): Energy {
        return Energy(
            amountSI + increment.amountSI,
            SI_UNIT
        )
    }
}

@Component
class PositiveOrZeroEnergyValidator : ConstraintValidator<PositiveOrZero, Energy> {
    override fun isValid(energy: Energy?, ctx: ConstraintValidatorContext?): Boolean {
        return energy?.amount(Energy.SI_UNIT)?.let { it >= 0 } ?: false
    }
}

@Component
class PositiveEnergyValidator : ConstraintValidator<Positive, Energy> {
    override fun isValid(energy: Energy?, ctx: ConstraintValidatorContext?): Boolean {
        return energy?.amount(Energy.SI_UNIT)?.let { it > 0 } ?: false
    }
}
