package com.katonaaron.energy.infrastructure.identity

import org.springframework.stereotype.Component
import java.util.*

@Component
class UUIDGeneratorImpl : UUIDGenerator {
    override fun generateUUID(): String = UUID.randomUUID().toString()
}
