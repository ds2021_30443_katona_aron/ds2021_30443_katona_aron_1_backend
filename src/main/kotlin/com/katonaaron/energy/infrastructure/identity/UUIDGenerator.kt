package com.katonaaron.energy.infrastructure.identity

interface UUIDGenerator {
    fun generateUUID(): String
}
