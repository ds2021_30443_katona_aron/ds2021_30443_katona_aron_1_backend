package com.katonaaron.energy.infrastructure.security.jwt

import io.jsonwebtoken.Jwts
import io.jsonwebtoken.security.Keys
import org.springframework.http.HttpHeaders
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter
import java.io.IOException
import javax.servlet.FilterChain
import javax.servlet.ServletException
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

class JwtAuthorizationFilter(
    authManager: AuthenticationManager?,
    private val jwtKey: String,
    private val jwtIssuer: String,
    private val jwtType: String,
    private val jwtAudience: String,
    private val userDetailsService: UserDetailsService
) : BasicAuthenticationFilter(authManager) {
    @Throws(IOException::class, ServletException::class)
    override fun doFilterInternal(
        request: HttpServletRequest,
        response: HttpServletResponse,
        chain: FilterChain
    ) {
        val authentication = parseToken(request)
        if (authentication != null) {
            SecurityContextHolder.getContext().authentication = authentication
        } else {
            SecurityContextHolder.clearContext()
        }
        chain.doFilter(request, response)
    }

    private fun parseToken(request: HttpServletRequest): UsernamePasswordAuthenticationToken? {
        val token = request.getHeader(HttpHeaders.AUTHORIZATION)
        if (token != null && token.startsWith("Bearer ")) {
            val claims = Jwts
                .parserBuilder()
                .setSigningKey(Keys.hmacShaKeyFor(jwtKey.toByteArray()))
                .build()
                .parseClaimsJws(token.replace("Bearer ", ""))
                .body
            return if (claims != null) {
                val user = userDetailsService.loadUserByUsername(claims.subject)
                UsernamePasswordAuthenticationToken(user, null, user.authorities)
            } else {
                null
            }
        }
        return null
    }
}
