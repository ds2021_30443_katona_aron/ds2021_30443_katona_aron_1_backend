package com.katonaaron.energy.infrastructure.security

import com.katonaaron.energy.application.useraccount.CreateUserAccount
import com.katonaaron.energy.domain.useraccount.UserAccounts
import com.katonaaron.energy.domain.useraccount.UserRole
import org.springframework.boot.CommandLineRunner
import org.springframework.stereotype.Component

@Component
class DefaultAdminAccountCreator(
    private val userAccounts: UserAccounts,
    private val createUserAccount: CreateUserAccount
) : CommandLineRunner {
    override fun run(vararg args: String?) {
        if (userAccounts.hasNoAccount()) {
            createUserAccount.createUserAccount(
                "admin",
                "admin123",
                UserRole.ROLE_ADMIN
            )
        }
    }
}
