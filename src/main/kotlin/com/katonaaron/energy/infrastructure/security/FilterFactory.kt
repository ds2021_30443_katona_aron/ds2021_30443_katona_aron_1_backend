package com.katonaaron.energy.infrastructure.security

import org.springframework.security.authentication.AuthenticationManager
import javax.servlet.Filter

interface FilterFactory {
    fun createAuthenticationFilter(authenticationManager: AuthenticationManager): Filter
    fun createAuthorizationFilter(authenticationManager: AuthenticationManager): Filter
}
