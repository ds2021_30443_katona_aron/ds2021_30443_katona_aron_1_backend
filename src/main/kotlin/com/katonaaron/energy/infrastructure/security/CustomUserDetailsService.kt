package com.katonaaron.energy.infrastructure.security

import com.katonaaron.energy.domain.useraccount.UserAccounts
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Service

@Service
class CustomUserDetailsService(
    private val userAccounts: UserAccounts
) : UserDetailsService {

    override fun loadUserByUsername(username: String?): UserDetails =
        username?.let {
            userAccounts.findByUsername(username)
        } ?: throw UsernameNotFoundException(username)
}
