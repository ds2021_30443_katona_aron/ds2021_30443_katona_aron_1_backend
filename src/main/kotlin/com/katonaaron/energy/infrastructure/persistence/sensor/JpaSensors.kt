package com.katonaaron.energy.infrastructure.persistence.sensor

import com.katonaaron.energy.domain.DDD
import com.katonaaron.energy.domain.sensor.Sensor
import com.katonaaron.energy.domain.sensor.SensorId
import com.katonaaron.energy.domain.sensor.Sensors
import com.katonaaron.energy.infrastructure.identity.UUIDGenerator
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Transactional

@DDD.RepositoryImpl
@Repository
@Transactional
class JpaSensors(
    private val repo: SensorRepositoryJpa,
    private val uuidGenerator: UUIDGenerator
) : Sensors {
    override fun nextIdentity(): SensorId = SensorId(uuidGenerator.generateUUID())

    override fun save(sensor: Sensor): Sensor = repo.saveAndFlush(sensor)

    override fun findAll(): Collection<Sensor> = repo.findAll()

    override fun findById(id: SensorId): Sensor? = repo.findById(id).orElse(null)

    override fun delete(sensor: Sensor) = repo.delete(sensor)

    override fun existsById(id: SensorId) = repo.existsById(id)
}
