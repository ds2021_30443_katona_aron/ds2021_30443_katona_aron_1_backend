package com.katonaaron.energy.infrastructure.persistence.sensor

import com.katonaaron.energy.domain.sensor.Sensor
import com.katonaaron.energy.domain.sensor.SensorId
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface SensorRepositoryJpa : JpaRepository<Sensor, SensorId>
