package com.katonaaron.energy.infrastructure.persistence.client

import com.katonaaron.energy.domain.DDD
import com.katonaaron.energy.domain.client.Client
import com.katonaaron.energy.domain.client.ClientId
import com.katonaaron.energy.domain.client.Clients
import com.katonaaron.energy.infrastructure.identity.UUIDGenerator
import org.springframework.stereotype.Repository

@DDD.RepositoryImpl
@Repository
class JpaClients(
    private val repo: ClientRepositoryJpa,
    private val uuidGenerator: UUIDGenerator
) : Clients {
    override fun nextIdentity(): ClientId = ClientId(uuidGenerator.generateUUID())

    override fun save(client: Client): Client = repo.saveAndFlush(client)

    override fun findAll(): Collection<Client> = repo.findAll()

    override fun findById(id: ClientId): Client? = repo.findById(id).orElse(null)

    override fun delete(client: Client) = repo.delete(client)
}
