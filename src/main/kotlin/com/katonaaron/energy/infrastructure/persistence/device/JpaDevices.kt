package com.katonaaron.energy.infrastructure.persistence.device

import com.katonaaron.energy.domain.DDD
import com.katonaaron.energy.domain.device.Device
import com.katonaaron.energy.domain.device.DeviceId
import com.katonaaron.energy.domain.device.Devices
import com.katonaaron.energy.infrastructure.identity.UUIDGenerator
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Transactional

@DDD.RepositoryImpl
@Repository
@Transactional
class JpaDevices(
    private val repo: DeviceRepositoryJpa,
    private val uuidGenerator: UUIDGenerator
) : Devices {
    override fun nextIdentity(): DeviceId = DeviceId(uuidGenerator.generateUUID())

    override fun save(device: Device): Device = repo.saveAndFlush(device)

    override fun findAll(): Collection<Device> = repo.findAll()

    override fun findById(id: DeviceId): Device? = repo.findById(id).orElse(null)

    override fun delete(device: Device) = repo.delete(device)

    override fun existsById(id: DeviceId) = repo.existsById(id)
}
