package com.katonaaron.energy.application.sensors

import com.katonaaron.energy.domain.device.Device
import com.katonaaron.energy.domain.device.DeviceId
import com.katonaaron.energy.domain.energy.Energy
import com.katonaaron.energy.domain.energy.EnergyUnit
import com.katonaaron.energy.domain.sensor.Sensor
import com.katonaaron.energy.domain.sensor.SensorId
import com.katonaaron.energy.domain.sensor.Sensors
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.AdditionalAnswers
import org.mockito.InjectMocks
import org.mockito.Mockito.*
import org.mockito.junit.jupiter.MockitoExtension
import org.mockito.kotlin.any

@ExtendWith(MockitoExtension::class)
internal class DeleteSensorUnitTest {

    private val sensors: Sensors = mock(Sensors::class.java)

    @InjectMocks
    private lateinit var deleteSensor: DeleteSensor

    private val sensorId = SensorId("1")
    private val sensor = Sensor(
        sensorId,
        "Description",
        Energy(1, EnergyUnit.kWh)
    )
    val device = Device(
        DeviceId("2"),
        "Description",
        "address",
        Energy(3, EnergyUnit.kWh),
        Energy(2, EnergyUnit.kWh)
    )

    @BeforeEach
    fun setUp() {
        sensor.attachToDevice(device)
        device.attachSensor(sensor)

        `when`(sensors.findById(sensor.id)).thenReturn(sensor)
        `when`(sensors.save(any())).then(AdditionalAnswers.returnsFirstArg<Sensor>())
    }

    @AfterEach
    fun tearDown() {
    }

    @Test
    fun `when delete sensor it should delete the sensor`() {
        deleteSensor.deleteSensor(sensorId)
        verify(sensors, times(1)).delete(sensor)
    }

    @Test
    fun `when delete sensor it should detach the sensor`() {
        deleteSensor.deleteSensor(sensorId)
        assertFalse(device.hasSensor())
    }
}
