FROM openjdk:11-jre-slim

ENV TZ=UTC

ARG DB_IP=localhost
ENV DB_IP=$DB_IP
ARG DB_PORT=5432
ENV DB_PORT=$DB_PORT
ARG DB_USER=user
ENV DB_USER=$DB_USER
ARG DB_PASSWORD=pass
ENV DB_PASSWORD=$DB_PASSWORD
ARG DB_DBNAME=energy
ENV DB_DBNAME=$DB_DBNAME
ARG PROD_SERVER=localhost
ARG PROD_SERVER=$PROD_SERVER


RUN mkdir /app

COPY ./build/libs/*.jar /app/spring-boot-application.jar

EXPOSE 8080

ENTRYPOINT ["java","-jar","/app/spring-boot-application.jar", "-XX:+UseContainerSupport -XX:+UnlockExperimentalVMOptions -XX:+UseCGroupMemoryLimitForHeap -XX:MaxRAMFraction=1 -Xms512m -Xmx512m -XX:+UseG1GC -XX:+UseSerialGC -Xss512k -XX:MaxRAM=72m", "-Djava.security.egd=file:/dev/./urandom"]
